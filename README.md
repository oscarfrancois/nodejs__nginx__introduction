# Découverte de nodejs

Introduction:

Nodejs est en environnement d'exécution de code javascript.
Comme pour votre navigateur, nodejs va donc exécuter du code javascript pour faire 
fonctionner votre projet.
A la différence près que votre navigateur va en général exécuter du code 
javascript dans le cadre d'une page HTML alors que nodejs va directement 
exécuter un programme complet en Javascript.
Nodejs fonctionne séparément de votre navigateur.

Nodejs sera utilisé principalement côté serveur typiquement pour fournir des 
services de type API à une application cliente de type navigateur web.


# Objectif global

Nous illustrerons l'utilisation de nodejs en réalisant un moteur de recherche minimaliste.


# Etat d'esprit de cette activité de découverte

Il vous est demandé d'être autonome dans la découverte de nodejs. Lors de chaque étape, commencez par chercher par vous même des solutions 
(faites appel à vos connaissances actuelles, moteur de recherche, tutoriaux, etc) vous permettant de réaliser les étapes demandées.
En cas de blocage, vous pouvez bien entendu demander de l'aide.


# Etapes à réaliser

Il vous est demandé de:

* Mettre en place un environnement serveur sous plateforme raspberry à base de système d'exploitation Linux (raspbian) permettant l'exécution de nodejs

Remarque: Un environnement de type machine virtuelle dédiée avec accès SSH complet peut éventuellement être utilisé en remplacement du raspberry. Veuillez dans ce cas utiliser un OS de type Debian avec la dernière version stable.

Il faudra notamment flasher une image raspbian
(https://www.raspberrypi.org/downloads/),

mettre à jour les paquets du système (attention au temps de mise à jour qui peut être conséquent):
`sudo apt update && apt upgrade`

Télécharger nodejs pour la bonne plateforme
( https://nodejs.org/en/download/ )
l'extraire, le rendre exécutable et rajouter ce chemin dans la variable système PATH.

Critère validant la réalisation de cette étape:
en ouvrant un terminal sur votre environnement raspberry/linux, l'exécution de la commande suivante:
  `node -e "console.log('OK');"`
Doit retourner le message:
  `OK`


* Activer un service web seulement à base de nodejs

Basez vous pour cela sur le module express de nodejs (cf. https://expressjs.com)

Critère validant la réalisation de cette étape:

Pouvoir naviguer depuis un ordinateur distant sur votre site web nodejs

Remarque: à ce stade, votre application nodejs peut écouter sur un port > 80
  En effet, il faut des droits particuliers pour être autorisé à écouter sur les port réseau < 1024


* Installer et configurer un service web basé sur nginx

Il faudra notamment installer le paquet nginx correspondant:
  `sudo apt install -y nginx`

Critère validant la réalisation de cette étape:
  en lancant un navigateur en local sur votre environnement raspberry/linux, l'URL suivante doit être atteignable:
  `http://localhost`

* Adapter la configuration du serveur web nginx pour rediriger les requêtes du sous ensemble /api 
vers votre application nodejs

Le concept mis en oeuvre s'appelle: reverse proxy.

Pour cela, il faudra modifier la configuration du serveur nginx.

Ce fichier de configuration se trouve par défaut sous:
`/etc/nginx/sites-available/default`

Exemple de configuration redirigeant l'URL /mynodejs vers le port 3000 en local (à adapter)

```
server {
    listen 80;    
    server_name _;
    location /mynodejs {
        proxy_pass http://localhost:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```

A chaque changement de configuration, il faut indiquer explicitement à nginx quand vous voulez qu'il la prenne en compte via la commande:
  `sudo systemctl reload nginx.service`

Critère validant la réalisation de cette étape:

effectuez les actions suffisantes pour que l'appel depuis un ordinateur distant à l'URL:
  http://A.B.C.D/api

  affiche une page web contenant le message:
  ```
  Message bien reçu de nodejs!
  ```

